# 1. Kotlin Null safety, smart-cast, safe-call, null coalescence.

- https://kotlinlang.org/docs/null-safety.html
- https://kotlinlang.org/docs/typecasts.html

## Null safety

Unlike in java, in kotlin null is explicit.
There are several operators to work with nullable objects:

- `?.` - safe-call operator
- `?:` - elvis operator
- `?.let {...}` - safe-call with let
- `!!` - not null assertion

To define a nullable variable:

```kotlin
var name: String? = null

// if it's null, the ?... part is skipped, if it's not null, only ? is skipped
println("The length of name is ${name?.length}")

// same thing can be done with ?.let:
name?.let {  // this block executes only if name is not null
  println("The length of name is ${name.length}")
}

val length = name?.length ?: -1  // len(name) if name is not None else -1
val notNullName = name ?: "Alex"  // notNullName = name or "Alex"

println("The length is ${name!!.length}")  // null ptr if name is null
```

The elvis operator can also be used to throw exceptions

## Smart-casts:

```kotlin
if (x is String) {
  // x is considered to be String in this block
}

if (x !is Int) return

// x is considered to be Int here

if (x !is String || x.length == 0) return  // x is casted to String after ||
// x is String and x.length > 0
```

## Type-casts:

```kotlin
val x: String = y as String  // unsafe
val x: String? = y as String?  // safe but nah
val x: String? = y as? String  // safe 😎
```

# 2. Immutable variables and immutable data types (collections).

https://kotlinlang.org/docs/collections-overview.html

```kotlin
val mutalbeList = mutableListOf(1, 2, 3)
val immutalbeList = listOf(1, 2, 3)
```

Same thing for set, map
Array is always mutable, as ArrayDeque

# 3. Kotlin OOP, inheritance, primary constructors.

https://kotlinlang.org/docs/inheritance.html

- primary constructor can be only one
- secondary constructors can be unlimited times
- secondary constructors must call primary constructor
- constructors might have default values
- `init` section is called after primary constructor
- `init` section doesn't take arguments
- multiple init sections are allowed
- primary constructor might have an access modifier
- `abstract` classes **can't be instantiated**
- `abstract` classes **can be inherited**
- `open` classes **can be inherited** as well
- other classes can't be inherited
- multiple inheritance is forbidden
- interfaces can be inherited
- multiple interface + abstract / open class might be inherited

# 4. Functional interface, SAM-conversion.

https://kotlinlang.org/docs/fun-interfaces.html

An interface with only one abstract method
is called a functional interface,
or a Single Abstract Method (SAM) interface.

The functional interface can have several
non-abstract members but only one abstract member.

```kotlin
// functional interface
fun interface IntPredicate {
    fun accept(i: Int): Boolean
}

// Creating an instance of a class without SAM conversion
val isEven = object : IntPredicate {
   override fun accept(i: Int): Boolean {
       return i % 2 == 0
   }
}

// With SAM conversion
val isEven = IntPredicate { it % 2 == 0 }
```

Comparison with type aliases:

```kotlin
typealias IntPredicate = (i: Int) -> Boolean

val isEven: IntPredicate = { it % 2 == 0 }

fun main() {
   println("Is 7 even? - ${isEven(7)}")
}
```

- type aliases are just aliases for existing types
- If your API needs to accept a function (any function)
  with some specific parameter and return types –
  use a simple functional type or define a type alias
  to give a shorter name to the corresponding functional type.
- If your API accepts a more complex entity than a function –
  for example, it has non-trivial contracts and/or
  operations on it that can't be expressed in a functional type's signature –
  declare a separate functional interface for it.

# 5. Kotlin properties, backing fields, derived properties, access modifiers.

Properties:

https://kotlinlang.org/docs/properties.html

- `var` attribute has a getter and a setter (you don't use attribute but a property instead)
- `val` attributes are read only (only provide getter)
- getters and setters can be overriden

Backing fields:

> In Kotlin, a field is only used as a part of a property
> to hold its value in memory. Fields cannot be declared directly.
> However, when a property needs a backing field,
> Kotlin provides it automatically.

This backing field can be referenced in the accessors
using the field identifier:

```kotlin
var counter = 0 // the initializer assigns the backing field directly
    set(value) {
        if (value >= 0)
            field = value
            // counter = value // ERROR StackOverflow: Using actual name 'counter' would make setter recursive
    }
```

The field identifier can only be used in the accessors of the property.

A backing field will be generated for a property
if it uses the default implementation of at least one of the accessors,
or if a custom accessor references it through the field identifier.

For example, there would be no backing field in the following case:

```kotlin
val isEmpty: Boolean
    get() = this.size == 0
```

Derived (delegated?) properties:

https://kotlinlang.org/docs/delegated-properties.html

Access modifiers:

https://kotlinlang.org/docs/visibility-modifiers.html

- private
- protected
- internal (in java package private)
- public (default)

# 6. Data Class, structural equality, referential equality.

https://kotlinlang.org/docs/data-classes.html
https://kotlinlang.org/docs/equality.html

- Equality (`==`) - equal by fields defined in data-class' primary constructor
- Referential equality (`===`) (a.k.a. identity) - reference to the same point in memory

We create data classes with keyword data:

```kotlin
data class User(val name: String, val age: Int)
```

The compiler automatically derives the following members
from all properties declared in the primary constructor:

- `equals()` / `hashCode()` pair
- `toString()` of the form `"User(name=John, age=42)"`
- `copy()` function (see below).

To ensure consistency and meaningful behavior of the generated code,
data classes have to fulfill the following requirements:

- The primary constructor needs to have at least one parameter.
- All primary constructor parameters need to be marked as `val` or `var`.
- Data classes cannot be `abstract`, `open`, `sealed`, or `inner`.

The compiler only uses the properties defined inside the primary constructor
for the automatically generated functions.
To exclude a property from the generated implementations,
declare it inside the class body:

```kotlin
data class Person(val name: String) {
    var age: Int = 0
}
```

Only the property `name` will be used inside the
`toString()`, `equals()`, `hashCode()`, and `copy()`
implementations, and there will only be one component function component1().
While two Person objects can have different ages, they will be treated as equal.

The standard library provides the `Pair` and `Triple` classes.
In most cases, though, named data classes are a better design choice
because they make the code more readable
by providing meaningful names for the properties.

# 7. Functional types, lambdas, trailing lambda, first-class functions.

https://en.wikipedia.org/wiki/First-class_function
https://kotlinlang.org/docs/lambdas.html

Kotlin functions are first-class, which means they can
be stored in variables and data structures,
and can be passed as arguments to and returned from other
[higher-order functions](#higher-order-functions).

You can perform any operations on functions
that are possible for other non-function values.

## Functional types

https://kotlinlang.org/docs/lambdas.html#function-types

Kotlin uses function types, such as `(Int) -> String`,
for declarations that deal with functions: `val onClick: () -> Unit = ...`

These types have a special notation that corresponds to the signatures
of the functions - their parameters and return values:

- All function types have a parenthesized list of parameter types and a return type:
  `(A, B) -> C` denotes a type that represents functions
  that take two arguments of types `A` and `B` and return a value of type `C`.
  The list of parameter types may be empty, as in () -> A.
  The `Unit` return type cannot be omitted. (`Unit` means no return value)
- Function types can optionally have an additional receiver type,
  which is specified before the dot in the notation:
  the type `A.(B) -> C` represents functions that can be called
  on a receiver object `A` with a parameter `B` and return a value `C`.
  Function literals with receiver are often used along with these types.
- Suspending functions belong to a special kind of function type
  that have a suspend modifier in their notation, such as
  `suspend () -> Unit` or `suspend A.(B) -> C`

The function type notation can optionally include names
for the function parameters: `(x: Int, y: Int) -> Point`.
These names can be used for documenting the meaning of the parameters.

To specify that a function type is nullable,
use parentheses as follows: `((Int, Int) -> Int)?`

You can also give a function type an alternative name by name by using a type alias:

```kotlin
typealias ClickHandler = (Button, ClickEvent) -> Unit
```

### Instantiating a function type

There are several ways to obtain an instance of a function type:

- Use a code block within a function literal, in one of the following forms:

  - a lambda expression: `{ a, b -> a + b }`
  - an anonymous function: `fun(s: String): Int { return s.toIntOrNull() ?: 0 }`

  Function literals with receiver can be used
  as values of function types with receiver

- Use a callable reference to an existing declaration:

  - a top-level, local, member, or extension function: `::isOdd`, `String::toInt`
  - a top-level, member, or extension property: `List<Int>::size`
  - a constructor: `::Regex`

- Use instances of a custom class that implements a function type as an interface

  ```kotlin
  class IntTransformer: (Int) -> Int {
      override operator fun invoke(x: Int): Int = TODO()
  }

  val intFunction: (Int) -> Int = IntTransformer()
  ```

### Invoking a function type instance:

A value of a function type can be invoked by using its `invoke(...)` operator:
`f.invoke(x)` or just `f(x)`.

## Lambdas

https://kotlinlang.org/docs/lambdas.html#lambda-expressions-and-anonymous-functions

Lambda expressions and anonymous functions are _function literals_.
Function literals are functions that are not declared
but are passed immediately as an expression.
Consider the following example:

```kotlin
max(strings, { a, b -> a.length < b.length })
```

The function `max` is a higher-order function,
as it takes a function value as its second argument.
This second argument is an expression that is itself a function,
called a function literal, which is equivalent to the following named function:

```kotlin
fun compare(a: String, b: String): Boolean = a.length < b.length
```

### Lambda expression syntax

The full syntactic form of lambda expressions is as follows:

```kotlin
val sum: (Int, Int) -> Int = { x: Int, y: Int -> x + y }
```

- A lambda expression is always surrounded by curly braces.
- Parameter declarations in the full syntactic form go inside
  curly braces and have optional type annotations.
- The body goes after the `->`.
- If the inferred return type of the lambda is not `Unit`,
  the last (or possibly single) expression inside the lambda body
  is treated as the return value.

If you leave all the optional annotations out, what's left looks like this:

```kotlin
val sum = { x: Int, y: Int -> x + y }
```

### Passing trailing lambdas

According to Kotlin convention,
if the last parameter of a function is a function,
then a lambda expression passed as the corresponding argument
can be placed outside the parentheses:

```kotlin
val product = items.fold(1) { acc, e -> acc * e }
```

Such syntax is also known as _trailing lambda_.

If the lambda is the only argument in that call,
the parentheses can be omitted entirely:

```kotlin
run { println("...") }
```

### `it`: implicit name of a single parameter

It's very common for a lambda expression to have only one parameter.

If the compiler can parse the signature without any parameters, the parameter does not need to be declared and `->` can
be omitted. The parameter will be implicitly declared under the name `it`:

```kotlin
ints.filter { it > 0 } // this literal is of type '(it: Int) -> Boolean'
```

### Returning a value from a lambda expression

You can explicitly return a value from the lambda using the qualified return syntax.
Otherwise, the value of the last expression is implicitly returned.

Therefore, the two following snippets are equivalent:

```kotlin
ints.filter {
    val shouldFilter = it > 0
    shouldFilter
}

ints.filter {
    val shouldFilter = it > 0
    return@filter shouldFilter
}
```

This convention allows for LINQ-style code:

```kotlin
strings.filter { it.length == 5 }.sortedBy { it }.map { it.uppercase() }
```

### Underscore for unused variables

If the lambda parameter is unused, you can place an underscore instead of its name:

```kotlin
map.forEach { (_, value) -> println("$value!") }
```

# 8. High-order functions. Examples.

https://kotlinlang.org/docs/lambdas.html#higher-order-functions

A higher-order function is a function
that takes functions as parameters, or returns a function.

A good example of a higher-order function is the
[functional programming idiom `fold`](<https://en.wikipedia.org/wiki/Fold_(higher-order_function)>)
for collections. It takes an initial accumulator value
and a combining function and builds its return value by consecutively
combining the current accumulator value with each collection element,
replacing the accumulator value each time:

```kotlin
fun <T, R> Collection<T>.fold(
    initial: R,
    combine: (acc: R, nextElement: T) -> R
): R {
    var accumulator: R = initial
    for (element: T in this) {
        accumulator = combine(accumulator, element)
    }
    return accumulator
}
```

In the code above, the `combine` parameter
has the function type `(R, T) -> R`, so it accepts a function that
takes two arguments of types `R` and `T` and returns a value of type `R`.
It is invoked inside the `for` loop,
and the return value is then assigned to `accumulator`.

To call `fold`, you need to pass an instance of the function type to it as an argument,
and lambda expressions are widely used for this purpose at
higher-order function call sites:

```kotlin
fun main() {
    //sampleStart
    val items = listOf(1, 2, 3, 4, 5)

    // Lambdas are code blocks enclosed in curly braces.
    items.fold(0, {
        // When a lambda has parameters, they go first, followed by '->'
        acc: Int, i: Int ->
        print("acc = $acc, i = $i, ")
        val result = acc + i
        println("result = $result")
        // The last expression in a lambda is considered the return value:
        result
    })

    // Parameter types in a lambda are optional if they can be inferred:
    val joinedToString = items.fold("Elements:", { acc, i -> acc + " " + i })

    // Function references can also be used for higher-order function calls:
    val product = items.fold(1, Int::times)
    //sampleEnd
    println("joinedToString = $joinedToString")
    println("product = $product")
}
```

The function `max` is a higher-order function,
as it takes a function value as its second argument.
This second argument is an expression that is itself a function,
called a function literal, which is equivalent to the following named function:

```kotlin
max(strings, { a, b -> a.length < b.length })
```

### Custom example

```kotlin
fun higherfunc( callback: () -> Unit ) {  // accepting lambda as parameter
    callback()                            // invokes lambda expression
}
fun main(args: Array<String>) {
    val lambda = {println("Hello world") }
     //invoke higher-order function
    higherfunc(lambda)                // passing lambda as parameter
}
```

# 9. Extension functions. Extension functions with generic receiver.

https://kotlinlang.org/docs/extensions.html
https://kotlinlang.org/docs/generics.html

## Extenstion functions

Kotlin provides the ability to extend a class or an interface with new functionality
without having to inherit from the class or use design patterns such as _Decorator_.
This is done via special declarations called _extensions_.

For example, you can write new functions for a class or an interface
from a third-party library that you can't modify.

Such functions can be called in the usual way,
as if they were methods of the original class.
This mechanism is called an _extension function_.

There are also _extension properties_ that let you define
new properties for existing classes.

### Extension functions

To declare an extension function, prefix its name with a _receiver type_,
which refers to the type being extended.

The following adds a `swap` function to `MutableList<Int>`:

```kotlin
fun MutableList<Int>.swap(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' corresponds to the list
    this[index1] = this[index2]
    this[index2] = tmp
}
```

The `this` keyword inside an extension function corresponds to the receiver object
(the one that is passed before the dot).
Now, you can call such a function on any `MutableList<Int>`:

```kotlin
val list = mutableListOf(1, 2, 3)
list.swap(0, 2) // 'this' inside 'swap()' will hold the value of 'list'
```

This function makes sense for any `MutableList<T>`, and you can make it generic:

```kotlin
fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' corresponds to the list
    this[index1] = this[index2]
    this[index2] = tmp
}
```

You need to declare the generic type parameter
before the function name to make it available in the receiver type expression.
For more information about generics, see [generic functions](#generic-functions).

### Extensions are resolved _statically_

Extensions do not actually modify the classes they extend. By defining an extension, you are not inserting new members into
a class, only making new functions callable with the dot-notation on variables of this type.

Extension functions are dispatched _statically_, which means they are not virtual by receiver type.
An extension function being called is determined by the type of the expression on which the function is invoked,
not by the type of the result from evaluating that expression at runtime. For example:

```kotlin
fun main() {
//sampleStart
    open class Shape
    class Rectangle: Shape()

    fun Shape.getName() = "Shape"
    fun Rectangle.getName() = "Rectangle"

    fun printClassName(s: Shape) {
        println(s.getName())
    }

    printClassName(Rectangle())
//sampleEnd
}
```

This example prints _Shape_, because the extension function called depends only
on the declared type of the parameter `s`, which is the `Shape` class.

If a class has a member function, and an extension function is defined
which has the same receiver type, the same name, and is applicable to given arguments,
the _member always wins_. For example:

```kotlin
fun main() {
//sampleStart
    class Example {
        fun printFunctionType() { println("Class method") }
    }

    fun Example.printFunctionType() { println("Extension function") }

    Example().printFunctionType()
//sampleEnd
}
```

This code prints _Class method_.

However, it's perfectly OK for extension functions to overload member functions
that have the same name but a different signature:

```kotlin
fun main() {
//sampleStart
    class Example {
        fun printFunctionType() { println("Class method") }
    }

    fun Example.printFunctionType(i: Int) { println("Extension function #$i") }

    Example().printFunctionType(1)
//sampleEnd
}
```

### Nullable receiver

Note that extensions can be defined with a nullable receiver type.
These extensions can be called on an object variable
even if its value is null, and they can check for `this == null` inside the body.

This way, you can call `toString()` in Kotlin without checking for `null`,
as the check happens inside the extension function:

```kotlin
fun Any?.toString(): String {
    if (this == null) return "null"
    // after the null check, 'this' is autocast to a non-null type, so the toString() below
    // resolves to the member function of the Any class
    return toString()
}
```

### Extension properties

Kotlin supports extension properties much like it supports functions:

```kotlin
val <T> List<T>.lastIndex: Int
    get() = size - 1
```

> Since extensions do not actually insert members into classes, there's no efficient way for an extension
> property to have a backing field.
> This is why _initializers are not allowed for extension properties_.
> Their behavior can only be defined by explicitly providing getters/setters.
> Example:

```kotlin
val House.number = 1 // error: initializers are not allowed for extension properties
```

### Companion object extensions

If a class has a companion object defined, you can also define extension
functions and properties for the companion object.
Just like regular members of the companion object,
they can be called using only the class name as the qualifier:

```kotlin
class MyClass {
    companion object { }  // will be called "Companion"
}
fun MyClass.Companion.printCompanion() { println("companion") }
fun main() {
    MyClass.printCompanion()
}
```

### Scope of extensions

In most cases, you define extensions on the top level, directly under packages:

```kotlin
package org.example.declarations
fun List<String>.getLongestString() { /*...*/}
```

To use an extension outside its declaring package, import it at the call site:

```kotlin
package org.example.usage
import org.example.declarations.getLongestString
fun main() {
    val list = listOf("red", "green", "blue")
    list.getLongestString()
}
```

### Declaring extensions as members

You can declare extensions for one class inside another class.
Inside such an extension, there are multiple _implicit receivers_ -
objects whose members can be accessed without a qualifier.
An instance of a class in which the extension is declared is called a _dispatch receiver_,
and an instance of the receiver type of the extension method is called an _extension receiver_.

```kotlin
class Host(val hostname: String) {
    fun printHostname() { print(hostname) }
}
class Connection(val host: Host, val port: Int) {
    fun printPort() { print(port) }
    fun Host.printConnectionString() {
        printHostname()   // calls Host.printHostname()
        print(":")
        printPort()   // calls Connection.printPort()
    }
    fun connect() {
        /*...*/
        host.printConnectionString()   // calls the extension function
    }
}
fun main() {
    Connection(Host("kotl.in"), 443).connect()
    //Host("kotl.in").printConnectionString()  // error, the extension function is unavailable outside Connection
}
```

In the event of a name conflict between the members of a dispatch receiver
and an extension receiver, the extension receiver takes precedence.
To refer to the member of the dispatch receiver,
you can use the qualified `this` syntax

```kotlin
class Connection {
    fun Host.getConnectionString() {
        toString()         // calls Host.toString()
        this@Connection.toString()  // calls Connection.toString()
    }
}
```

Extensions declared as members can be declared as `open` and overridden in subclasses.
This means that the dispatch of such functions is virtual
with regard to the dispatch receiver type, but static with regard
to the extension receiver type.

```kotlin
open class Base { }
class Derived : Base() { }
open class BaseCaller {
    open fun Base.printFunctionInfo() {
        println("Base extension function in BaseCaller")
    }
    open fun Derived.printFunctionInfo() {
        println("Derived extension function in BaseCaller")
    }
    fun call(b: Base) {
        b.printFunctionInfo()   // call the extension function
    }
}
class DerivedCaller: BaseCaller() {
    override fun Base.printFunctionInfo() {
        println("Base extension function in DerivedCaller")
    }
    override fun Derived.printFunctionInfo() {
        println("Derived extension function in DerivedCaller")
    }
}
fun main() {
    BaseCaller().call(Base())   // "Base extension function in BaseCaller"
    DerivedCaller().call(Base())  // "Base extension function in DerivedCaller" - dispatch receiver is resolved virtually
    DerivedCaller().call(Derived())  // "Base extension function in DerivedCaller" - extension receiver is resolved statically
}
```

### Note on visibility

Extensions utilize the same visibility modifiers as regular functions
declared in the same scope would.
For example:

- An extension declared at the top level of a file has access to the other `private`
  top-level declarations in the same file.
- If an extension is declared outside its receiver type,
  it cannot access the receiver's `private` or `protected` members.

## Generic functions

Classes aren't the only declarations that can have type parameters.
Functions can, too. Type parameters are placed _before_ the name of the function:

```kotlin
fun <T> singletonList(item: T): List<T> {
    // ...
}
fun <T> T.basicToString(): String { // extension function
    // ...
}
```

To call a generic function, specify the type arguments at the call site
_after_ the name of the function:

```kotlin
val l = singletonList<Int>(1)
```

Type arguments can be omitted if they can be inferred from the context,
so the following example works as well:

```kotlin
val l = singletonList(1)
```

# 10. \* Parametric Polymorphism (Generics) declaration-site variance

I am very sorry, but я чуть-чуть буду писать по-русски

https://kotlinlang.org/docs/generics.html

### Вступление: что такое Дженерики

Classes in Kotlin can have type parameters, just like in Java:

```kotlin
class Box<T>(t: T) {
    var value = t
}
```

To create an instance of such a class, simply provide the type arguments:

```kotlin
val box: Box<Int> = Box<Int>(1)
```

Generics are good to increase API flexibility (we do not need to write functionallity for every possible data-type, we just write it as generic and it just works fine). e.g.

```java
// Java
interface Collection<E> ... {
    void addAll(Collection<E> items);
}
// but this fucking sucks, because
```

```java
// Java
void copyAll(Collection<Object> to, Collection<String> from) {
    to.addAll(from);
    // !!! Would not compile with the naive declaration of addAll:
    // Collection<String> is not a subtype of Collection<Object>
}
```

That's why the actual signature of addAll() is the following:

```java
// Java
interface Collection<E> ... {
    void addAll(Collection<? extends E> items);
}
```

this means, that ? should be a child of E, not actually E, which makes sense

### Ковариантность и контравариантность (важные 2 слова)!

Дженерики могут описывать 2 различных поведения:

1. Collection<? extends Object> - лежит ребёнок Object-a (covariance)

2. Collection<? super String> - лежит предок String-a (contavariance)

Что же это значит?

В случае номер (1) мы получаем коллекцию чего-то, что экстендит Object. Это называется upper-bound или covariance. Поскольку мы знаем верхнюю границу мы можем из этой коллекции читать (можем написать что-то типа:
```java
Object obj = collection.pop()
```

мы знаем, что в объект залезет то, что мы попнем)

напротив, мы не можем в коллекцию адекватно что-то записать, потому что мы не знаем, получится ли в нее записать тот тип, который мы хотим. Например попробуем записать String, а в коллекции на самом деле будут лежать Int-ы

В случае номер (2) мы получаем противоположную ситуацию: у нас есть lower-bound. Мы знаем нижнюю границу. Это называется контравариантность. Мы знаем, что класс можем потребить String, значит он также может потребить Object.


```kotlin
interface Comparable<in T> {
    operator fun compareTo(other: T): Int
}

fun demo(x: Comparable<Number>) {
    x.compareTo(1.0) // 1.0 has type Double, which is a subtype of Number
    // Thus, you can assign x to a variable of type Comparable<Double>
    val y: Comparable<Double> = x // OK!
}
```


# 11. \*\* Parametric Polymorphism (Generics) mixed-site variance

### Use-site variance (before we go to mixed-site)

Use-site variance is same as declaration-site variance, but we do it on use, not on declare.

We need it because some pieces of code will fail if we don't specify variance; for example:

```kotlin
fun copy(from: Array<Any>, to: Array<Any>) {
    assert(from.size == to.size)
    for (i in from.indices)
        to[i] = from[i]
}

val ints: Array<Int> = arrayOf(1, 2, 3)
val any = Array<Any>(3) { "" }
copy(ints, any)
//   ^ type is Array<Int> but Array<Any> was expected
```

in order for this to work:

```kotlin
fun copy(from: Array<out Any>, to: Array<Any>) { ... }
```

this way compiler knows that we won't put shit to our "from" array

### Mixed-site variance

This is when we mix declaration-site and use-site

To implement subtyping between parameterized types, Kotlin uses mixed-site variance — a combination of declaration- and use-site variance, which is easier to understand and reason about, compared to wildcards from Java. Mixed-site variance means you can specify, whether you want your parameterized type to be co-, contra- or invariant on some type parameter, both in type parameter (declaration-site) and type argument (use-site).

Info: variance is a way of describing how subtyping works for variant parameterized types. 

In declaration-site:

A <: B ->

if T<out F>: T<A> <: T<B>

if T<in F>: T<A> :> T<B>

if T<F>: T<A> nothing T<B>

With use-site variance we can specify on different sides of equation

T<out A> <: T<out B>

T<in A> :> T<in B>

T<A> <: T<out A>

T<A> <: T<in A>

Также работает правило транизитвности
